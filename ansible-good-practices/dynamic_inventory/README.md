# Dynamic Inventories

## 1: Create your infra with Terraform
```cd terraform```

```terraform init```

```terraform apply```

## 2: List your hosts
```ansible-inventory -i inventory/aws --graph``` 

## 3: Run your role
```ansible-playbook -u ubuntu deploy-ec2.yml``` 
