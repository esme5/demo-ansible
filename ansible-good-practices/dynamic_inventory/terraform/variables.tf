# ------------------  utils  ---------------------------

variable "region" {
  default = "eu-west-1"
}

# ------------------ EC2 -----------------------------

variable "ami_id" {
  description = "id of an ami by default it's ubuntu 18.04"
  type        = string
  default     = "ami-01dd271720c1ba44f"
}

variable "instance_type" {
  description = "aws ec2 instance type"
  type        = string
  default     = "t2.micro"
}

variable "prod1_tag" {
  type        = string
  default = "prod1"
}

variable "prod2_tag" {
  type        = string
  default = "prod2"
}

variable "aws_public_key_ssh_path" {
  description = "The key name of the Key Pair to use for the instance"
  type        = string
}

variable "aws_private_key_ssh_path" {
  description = "The key name of the Key Pair to use for the instance"
  type        = string
}
