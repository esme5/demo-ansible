# Ansible Vault

First you need to create a **.ansible_vault_password_file.txt** file that contains your password

## Run with local var file
```ansible-playbook main.yml -e "@local.yml"```

## Encrypt vars
```ansible-vault encrypt local.yml``` 

## Decrypt vars
```ansible-vault decrypt local.yml``` 
