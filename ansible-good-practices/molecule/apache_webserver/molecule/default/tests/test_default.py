import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_httpd_packages(host):
    httpd = host.package("httpd")
    assert httpd.is_installed
    assert httpd.version.startswith("2.4.6")


def test_httpd_service(host):
    httpd = host.service("httpd")
    assert httpd.is_running
    assert httpd.is_enabled


def test_httpd_file(host):
    file = host.file("/var/www/html/test.html")
    assert file.exists
    assert oct(file.mode) == "0o640"
    assert file.user == "apache"
    assert file.group == "apache"


def test_http_port(host):
    assert host.socket("tcp://127.0.0.1:8080").is_listening

