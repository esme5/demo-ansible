# Molecule

## Install molecule
https://molecule.readthedocs.io/en/latest/installation.html

```pip install 'molecule[docker]'```

## Install testinfra

```pip install  pytest-testinfra```

## Init role with docker

```molecule init role --driver-name docker apache_webserver```